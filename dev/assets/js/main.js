const bdy = $('body');
      wih = $(window).height(),
      wit = $(window).width();

function toppadding() {
    var topsize = $('header').height();
    if (topsize > 123){
    }

    $(window).resize(function(){
        $('.owl-carousel').trigger('refresh.owl.carousel');
    });
    
}

(function($) {
    $.fn.inputFilter = function(inputFilter) {
      return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
      });
    };
  }(jQuery));

function topSearchAction() {
    $('.top-search').focus(function () {
        $(this).addClass('is-active');
        $('.search-main i').addClass('is-active');
    });

    $( document ).on( 'click', function ( e ) {
        if ( $( e.target ).closest( '.nav-search' ).length === 0 ) {
            $('.top-search').val('');
            $('.top-search').removeClass('is-active');
            $('.search-main i').removeClass('is-active');
        }
    });

}

function btnHide() {
    $('.sh-close').click(function(){
        $('#shoppingHistory').fadeOut(150, function () {
            $(this).remove();
        });
    })
}

function footerTextPreview() {
    $('.showFooterText').click(function () {
        $('.footer-about--hidden_tx').addClass('show');
        $(this).hide();
    });

    $('.hiddenFooterText').click(function () {
        $('.footer-about--hidden_tx').removeClass('show');
        $('.showFooterText').show();
    });
}

function cookieMainHide() {
    $("#homeMainCookie").click(function () {
        $(".cookie-main").fadeOut(150, function () {
            $(this).css('display','none');
        });
    });
}

function crouselSlide() {

   /**  GLOBAL WİTGET SLIDER */

   $('.carousel-slide').owlCarousel({
        dots: false,
        nav: true,
        items: 5,
        lazyLoad: true,
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
                dots: true,
                nav: false,
            },
            600:{
                items:3,
                dots: true,
                nav: false,
            },
            948:{
                items:4,
                nav: false,
            },
            1024:{
                items:5,
            },
            1200:{
                items:5,
            }
        }
    });

     /**  SHOPING HISTORY SLIDER */

   $('.shop-history-slider').owlCarousel({
        dots: false,
        nav: false,
        items: 3,
        lazyLoad: true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                dots: true,
            },
            600:{
                items:2,
                dots: true,
            },
            948:{
                items:2,
            },
            1024:{
                items:3,
            },
            1200:{
                items:3,
            }
        }
    });


    /**  CATEGORY PAGE BEST SELLER SLIDER */

   $('.best-seller').owlCarousel({
    dots: false,
    nav: false,
    items: 5,
    lazyLoad: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            dots: true,
        },
        600:{
            items:3,
            dots: true,
        },
        948:{
            items:4,
        },
        1024:{
            items:5,
        },
        1200:{
            items:5,
        }
    }
});
  
    /* CATEGORY PAGE SLIDER */
    $('.category-slider').owlCarousel({
        dots: false,
        nav: false,
        items: 1,
        lazyLoad: true,
    });


    /*HOME MAIN SLIDER*/

    var $sync1s = $(".slider");
    var duration = 300;
    
        $sync1s.owlCarousel({
            margin: 0,
            items: 1,
            loop: true,
            lazyLoad:true,
            autoplay: false,
            nav: false,
            dots: true,
            dotsData: true,
            responsive:{
                0:{
                    dotsData: false,
                },
                768:{
                    dotsData: false,
                },
                1024:{
                    dotsData: true,
                }
            }
            
        }).on('click', '.owl-dot', function () {
            $sync1s.trigger('to.owl.carousel', [$(this).index(), duration, true]);
    
        })

        $('.thumb-slider .owl-dot').hover(function() {
            $(this).click();
        }, function() {})


}

function navOpen() {
    $('nav').mouseover(function () {
        $('.nav-menu').addClass('is-open');
        $(this).mouseout(function () {
            $('.nav-menu').removeClass('is-open');
        });
    });

}

function mbNav() {
    $('#mb-nav').click(function () {
        $('.top-second').addClass('is-open');
        $('body').addClass('is-lock');
    });

    $('.btn-close').click(function () {
        $('.top-second').removeClass('is-open');
        $('body').removeClass('is-lock');
    });

    /*sub-menu*/
    var index = '.nav--item .nav--link';
    $(index).on('click', function () {
        $('.nav--item').removeClass('selected');
        $(this).parent().addClass('selected');

    });

    /*sub-menu 2*/
    var indexSub = '.mb-sub-2-head .dp-item--head';
    $(indexSub).on('click', function () {
        $('.mb-sub-2-head').removeClass('selected');
        $(this).parent().addClass('selected');

    });

    $('.backMenu').click(function () {
        $('.nav--item').removeClass('selected');
    });

    $('.backMenuSub').click(function () {
        $('.mb-sub-2-head').removeClass('selected');
    });

    $('.all_close').click(function () {
        $('.top-second').removeClass('is-open');
        $('.nav--item').removeClass('selected');
        $('.mb-sub-2-head').removeClass('selected');
        $('body').removeClass('is-lock');
        $('.overlay').hide();
    });


    /* search open/close */
    $('#search-icon').click(function (){
        $(this).toggleClass('is-open');
        $('.nav-search').toggleClass('is-open');
    });
}

function menuOverlay(){
    var $overlay = $('.overlay');
    $('.menu-main').bind("mouseenter", function(){
            $overlay.stop(true,true).fadeTo(200, 0.6);
        }).bind('mouseleave',function(){
            $overlay.stop(true,true).fadeTo(200, 0);
            $overlay.hide();
        })
}


var dtPhoto = function() {

    var bigCrousel = function(){

        $('.thumb-item').first().addClass('active-border');
        
        var $sync1 = $(".big--img"),
            $sync2 = $(".thumb--img"),
            flag = false,
            duration = 300;
        
        $sync1.owlCarousel({
                items: 1,
                margin: 10,
                nav: true,
                dots: false,
                responsiveClass:true,
                responsive:{
                    0:{
                        nav:false,
                        dots: true,
                    },
                    768:{
                        nav:false,
                        dots: true,
                    },
                    1024:{
                        nav:true,
                        dots: false,
                    }
                }
            })
            .on('changed.owl.carousel', function (e) {
                if (!flag) {
                    flag = true;
                    $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
                    $('.thumb-item').removeClass('active-border');
                    var indexs = e.item.index;
                   $('.thumb-item').eq(indexs).addClass('active-border');
                    flag = false;
                }
            });
        
        $sync2.owlCarousel({
                margin: 10,
                items: 6,
                nav: false,
                center: false,
                dots: false,
                navRewind: false
            })
            .on('click', '.owl-item', function () {
                $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        
            })
            .on('changed.owl.carousel', function (e) {
                if (!flag) {
                    flag = true;        
                    $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
                    flag = false;
                }
            });

            $('.thumb-item').hover(function() {
                $(this).click();
            }, function() {});

    };

    var lightboxBigCrousel = function(){

        
        var $sync1s = $(".lightbox-big--img");
        var duration = 300;
        
            $sync1s.owlCarousel({
                margin: 0,
                items: 1,
                lazyLoad:true,
                dots: true,
                dotsData: true,
                responsive:{
                    0:{
                        dotsData: false,
                    },
                    768:{
                        dotsData: false,
                    },
                    1024:{
                        dotsData: true,
                    }
                }
               
            }).on('click', '.owl-dot', function () {
                $sync1s.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        
            })
          
    };

    var bigpic = function(){
        const bpc = $('.light-main').height(), sliderImg = $('.lightbox-big--img img'), sliderMain = $('.lightbox-big--img .owl-item div.item');
        sliderImg.css({ 'height': (bpc - 80) + 'px' });
        sliderMain.css({ 'height': (bpc - 80) + 'px' });
        $(window).resize(function () {
            const bpc = $('.light-main').height();
            sliderImg.css({ 'height': (bpc - 80) + 'px' });
            sliderMain.css({ 'height': (bpc - 80) + 'px' });
        });
    };

    var init = function(){
            bigCrousel();
            lightboxBigCrousel();
            bigpic();
    }

    return{
        init:init
    };
}();


var ui = function(){

    var servicesItem = $('.vtn-special-item');
    var servicesText = $('.vtn-special-text p');

    var dtQquantityEntry = $('#quantity');

    let minQuentityVal = 1;
    let quentityVal = 1;

    
    var dtVtnServices = function(){
        servicesItem.mouseover(function(){
            var index = $(this).index();
            servicesItem.removeClass('active');
            servicesText.removeClass('active');
            $(this).addClass('active');
            servicesText.eq(index).addClass('active');
        });
        servicesItem.mouseout(function(){
            servicesItem.removeClass('active');
            servicesText.removeClass('active');
        });
    };

    var dtQuantity = function(){

        dtQquantityEntry.on("keypress blur drop",function (e) {    
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                 e.preventDefault();
                 $("#emessage").removeClass().stop();
                 $("#emessage").addClass('error').html("Sadece Rakam Kullanınız (0-9)").show();
                 setTimeout(function() { $("#emessage").removeClass('error').hide(); }, 5000);
                return false;
             }
            
            if($(this).val() == ''){
                parseInt($(this).val('1'));
            }

         });

        dtQquantityEntry.val(minQuentityVal);

        $('#increase').on('click', function(e){
            quentityVal = quentityVal + 1;
            dtQquantityEntry.val(quentityVal);
        });

        $('#decrease').on('click', function(e){
            e.preventDefault();

            quentityVal = quentityVal - 1;
            if(quentityVal < minQuentityVal){
                quentityVal = 1;
                $("#emessage").removeClass().stop();
                $("#emessage").addClass('warning').html("Minimum 1 adet olmalıdır.").show();
                 setTimeout(function() { $("#emessage").removeClass('warning').hide(); }, 5000);
            }
            dtQquantityEntry.val(quentityVal);
        });



    };


    const bundleCheck = $('.dt-bundle-list');

    var bundleCk = function(){
        const item = $('.check-item');

        item.each(function(){
            if($(this).hasClass('active')){
                console.log('aktif : ' + $(this).index());
                $(this).find('input').attr('checked','Checked');
                $(this).find('label').html('Seçili');
    
            }
        });
    };

   


    const stars = $('.rank-item');
    
    var ranking = function(){
        stars.click(function(){
            stars.removeClass('selected');
            $(this).addClass('selected');
            const val = $(this).attr('rel');
            $('input[name=comment-rank]').val(val);
        });
    }


    var init = function(){
        if(servicesItem.length){
            dtVtnServices();
        }
        if(dtQquantityEntry.length){
            dtQuantity();
        }
        if(bundleCheck.length){
            bundleCk();
        }
        if(stars.length){
            ranking();
        }
    };



    return{
        init:init
    }

}();


var libs = function(){

    var zooms = $('.zoom');
    
    var lazyl = function(){
        $(".lazyload").lazyload();
    };

    var vzoom = function(){
       zooms.zoom();
    }


    var init = function(){
      lazyl();
      if(zooms.length){
        vzoom();
      }
    };
    

    return{
        init:init
    }

}();


var components = function(){

    var tabhead = $('.tab-head a');

    const body = $('body'),
          lgOverlay = $('.lightbox-overlay'),
          lgContenct =   $('.light-main'),
          lgClickItem = $('.lightbox-item');
          lgClose = $('.lightboxCclose');

    var tabs = function(){

        var firstActive = function(e){
            var tabsAction = function(e){
                var activetab = tabhead.eq(e);
                var thisRel = activetab.attr('rel');
                activetab.addClass('active');
                $('#'+ thisRel).addClass('active'); 
            };
            if(e == null){
                tabsAction(0);
            }else{
                tabsAction(e)
            }
        }
        firstActive(2);

        tabhead.on('click', function (e) {
            e.preventDefault();
            tabhead.removeClass('active');
            $(this).addClass('active');
            target = $(this).attr('rel');
            $('.tab-content').not(target).removeClass('active');
            $('#'+ target).addClass('active');
        });
    };

    var modal = function(){

        lgClickItem.on( 'click', function ( e ) {
            body.addClass('locked');
            lgOverlay.fadeIn();
            lgContenct.fadeIn();
            e.stopPropagation();
            var index =  $(this).parent().index();
            $(".lightbox-big--img").trigger('to.owl.carousel', [index, 300, true]);
        });

        lgClose.click(function(){
            body.removeClass('locked');
            lgOverlay.hide();
            lgContenct.hide();
        });

        $( document ).on( 'click', function ( e ) {
            if ( $( e.target ).closest( lgContenct ).length === 0 ) {
                body.removeClass('locked');
                lgOverlay.fadeOut();
                lgContenct.fadeOut();
            }
        });
                        
        $( document ).on( 'keydown', function ( e ) {
            if ( e.keyCode === 27 ) {
                body.removeClass('locked');
                lgOverlay.fadeOut();
                lgContenct.fadeOut();
            }
        });

    };


      var init = function(){
        if(tabhead.length){
            tabs();
        }
        if(lgClickItem.length){
            modal();
        }
      };

      return{
          init:init
      }

}();


var listpageUI = function(){

    const filterCaption = $('.filter-caption'),
          filterInfoVideo = $('.filter-info-video'),
          filterPriceInput = $('.filter-price-input'),

          filterList = $('.list-filter'),
          closeMobileFilter = $('#closeMobileFilter'),
          filterLink = $('.filter-select li a'),
          mobileFilter = $('#mobileFilterLink');

    var filterAccordion = function(){
        filterCaption.click(function(){
            const alt = $(this).parent();
            alt.find('.filter-main').stop();
            alt.find('.filter-main').slideToggle(300);
             
            var clearClass = function () {
                if(alt.hasClass('active') && !$(this).hasClass('top-acc')){
                    alt.removeClass('active');
                }else{
                    alt.addClass('active');
                }
            }

            setTimeout(clearClass, 300);
        });


    }


    const listItem = $('.filter-select li');

    var openAccordion = function(){

        listItem.each(function(){
            if($(this).hasClass('active')){
                $(this).parents('.filter-box').addClass('active');
                $(this).parents('.filter-main').addClass('active');
            }
        });
    };


    var filterIVideo = function () {
        filterInfoVideo.magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }

    var filterPrice = function () {
        filterPriceInput.inputFilter(function(value) {
            return /^-?\d*$/.test(value); 
        });
    }

    var mobileFilterShow = function (){

        var mobileFilterClickOpened = function(){
            mobileFilter.click(function(){
                filterList.addClass('active');
                bdy.addClass('locked');
            });
        }

        var mobileFilterOpened = function(){
            filterList.addClass('active');
            bdy.addClass('locked');
        }

        var mobileFilterRemoved = function(){
            filterList.removeClass('active');
            bdy.removeClass('locked');
        }

        var mobileFilterClosed = function(){
            closeMobileFilter.click(function(){
                mobileFilterRemoved();
                mobileFilterStoragedClosed();
            });
        }

        
        var mobileFilterStoragedOpened = function () {
            filterLink.click(function(){
                if(wit <= 991){
                localStorage.setItem('mobileFilter','is_open');
                }else{
                    console.log('big-size');
                }
            });
        }


        var mobileFilterShw = function(){
            if(localStorage.getItem('mobileFilter') == 'is_open' && wit <= 991){
                mobileFilterOpened();
                console.log('loc true');
            }else{
                mobileFilterRemoved();
            }
        }

        var mobileFilterStoragedClosed = function(){
            localStorage.removeItem('mobileFilter');
        }

        mobileFilterClickOpened();
        mobileFilterClosed();
        mobileFilterStoragedOpened();
        mobileFilterShw();


    }
    
    var init = function(){
        if(filterCaption.length){
            filterAccordion();
        }
        if(filterInfoVideo.length){
            filterIVideo();
        }
        if(filterPriceInput.length){
            filterPrice();
        }
        if(mobileFilter.length){
            mobileFilterShow();
        }
        if(listItem.length){
            openAccordion();
        }
    };

    return{
        init:init
    }
}();


var hoverCarousel = function(){

    const sld = $('.sld'),
        gridBtn = $('.list-type');

    var _carousel = function(slidercarousel){
        slidercarousel.owlCarousel({
            nav : false,
            dots : true,
            lazyLoad: true,
            fluidSpeed: 0,
            smartSpeed: 0,
            items: 1,
        });

        slidercarousel.on( "mousemove", function( event ) {
            
            const   $item = $(this).find('.owl-item'),
                    itemL = Math.ceil($item.length),
                    itemW = Math.ceil(slidercarousel.width()),
                    lines = Math.ceil((itemW / itemL));
        
            const offset = $(this).offset();
            x = event.pageX- Math.floor(offset.left);
            y = event.pageY- Math.floor(offset.top)
            const hes = Math.ceil((x / lines));
            if(hes == 0 ){
                hes = 4;
            }
            $(this).trigger('to.owl.carousel', (hes - 1));
        });

        slidercarousel.on("mouseout", function(){
            $(this).trigger('to.owl.carousel', 0);
        });

    
    }


    var carousels = function(){
        sld.mouseenter(function(){
            const ths = $(this).find('img');
            if(ths.length > 1){
                if(!$(this).hasClass('product-pic-slider')){
                    $(this).addClass('product-pic-slider owl-carousel');
                    _carousel($(this));
                }
            }
        });
    }

    var listGrid = function(){
        gridBtn.click(function(){

            gridBtn.removeClass('active');
            $(this).addClass('active');
            const dataclass = $(this).attr('data-class');
            $('#gridMain').removeAttr('class').addClass(dataclass);
            $('.owl-carousel').trigger('refresh.owl.carousel');

        });
    }

    
    var init = function(){
        if(sld.length){
            carousels();
        }
        if(gridBtn.length){
            listGrid();
        }
    }

    return{
        init:init
    }

}();



var cooks = function(){

    const cookie = Cookies.get('compare'),
          prd = $('.comp-icon'),
          body = $('body');

    var bodyPad = function(fn){
        if(fn == 'add'){
            body.css('padding-bottom','78px');
        }else if(fn == 'remove'){
            body.css('padding-bottom','0');
        }
    };

    var addCompareArea = function(){
        body.append('<div class="compare-area"><div class="vtn-container">'+
            '<div class="compare-main"></div>'+
            '<div class="compare-action-area"><a href="javascript:void(0);" class="clear-compare">TEMİZLE</a>'+
            '<a href="javascript:void(0);" class="btn btn-lightb compare-start"><span class="icong-compare"></span> KARŞILAŞTIR</a></div>'+
            '</div></div>');
        
        $('.compare-action-area').append('<span class="kelime" style="display:none"></span>');
            
        bodyPad('add');
    };

    var compareItemList = function(){
        var del = Cookies.getJSON('compare');
        $.each(del, function(index,el) {
           
            if (index > 3) {
                return false;
            }
            $('.compare-main').append('<div class="compare-item" title="'+ el.name +'"><a href="'+ el.url +'"><img src="assets/images/products/'+ el.img +'"></a><div class="compare-item-info"><h3><a href="'+ el.url +'">'+ el.name +'</a></h3><h4 class="compare-price">'+ el.price +' TL</h4></div></div>');
            if(index > 0){
                $('.kelime').append('-'+el.url);
            }else{
                $('.kelime').append(el.url);
            }
        });
        var urls = 'https://www.vatanbilgisayar.com/compare/'+$('.kelime').html();
        $('.compare-start').attr('href',urls);
    };
    

    $('.comp-icon').on('click', function(){
        
        const cookie = Cookies.get('compare'),
              $this = $(this),
              name = $this.attr('data-name'),
              code = $this.attr('data-code'),
              id = $this.attr('data-id'),
              url = $this.attr('data-url'),
              category = $this.attr('data-category'),
              price = $this.attr('data-price'),
              img = $this.attr('data-img');
              

        if (cookie == undefined) {
            var obj = [{ name: name, url: url, code: code, id: id, category: category, price: price, img: img }];
            Cookies.set('compare', JSON.stringify(obj));
            
            addCompareArea();
            compareItemList();
        }
        else if(cookie != undefined && cookie.includes(id)) {
            alert('Ayrı ürünü bir daha ekleyemezsiniz.');
        }
        else if (cookie != undefined && !cookie.includes(category)) {
            alert('Farklı kategorideki ürünleri karşılaştıramazsınız.');
        }
        else {
            var thisCk = Cookies.getJSON('compare');
            if(thisCk.length > 3){
                alert('En fazla 4 ürünü karşılaştırabilirsiniz.');
            }
            else{
                var obj = { name: name, url: url, code: code, id: id, category: category, price: price, img: img };
                thisCk.push(obj);
                Cookies.set('compare', JSON.stringify(thisCk));
                
                $('.compare-item').remove();
                $('.kelime').html('');
                compareItemList();
            }
        }

    }); 

    
        if(!$('.compare-area').length && cookie != undefined){
            addCompareArea();
            compareItemList();
        }
        else if($('compare-area').length && cookie == undefined){
            $('.compare-area').remove();
            bodyPad('remove');
        }

        $(document).on('click', '.clear-compare', function(){
            Cookies.remove('compare');
            $('.kelime').html('');
            $('.compare-area').remove();
            bodyPad('remove');
        });
        
}



var uyeliksizSatis = function(){

    var faturaAyri = $('.faturaAyri'),
        kurumsalInput = $('.kurumsalInput'),
        bireyselInput = $('.bireyselInput');
    
    var faturaClick = function(){
        $('#fatura1').click(function(){
            faturaAyri.toggle();
        });
    };

    var kurumsalClick = function(){
        $('#kurumsal').click(function(){
            bireyselInput.hide();
            kurumsalInput.show()
        });
    };

    var bireyselClick = function(){
        $('#bireysel').click(function(){
            kurumsalInput.hide();
            bireyselInput.show();
            if($('#fatura1').prop('checked') == true){
                faturaAyri.hide();
            }
        });
    };

    if($('#fatura1').prop('checked') == true){
        faturaAyri.hide();
    }

    if($('#bireysel').prop('checked') == true){
        kurumsalInput.hide();
        if($('#fatura1').prop('checked') == true){
            faturaAyri.hide();
        }
    }else{
        kurumsalInput.show();
    }

    if($('#kurumsal').prop('checked') == true){
        bireyselInput.hide();
    }else{
        if($('#fatura1').prop('checked') == true){
            bireyselInput.show();
            faturaAyri.hide();
        }
        
    }


    var init = function(){
      if(faturaAyri.length){
        faturaClick();
      }
      bireyselClick();
      kurumsalClick();
    };
    

    return{
        init:init
    }

}();


var basketDropdown = function(){
    
    $('.basket-btn').mouseover(function(){
        $('.basket-open').show();
    });

    $('.basket-open').mouseover(function(){
        $('.basket-open').show();
    });

    $('.basket-btn').mouseout(function(){
        $('.basket-open').hide();
    });

    $('.basket-open').mouseout(function(){
        $('.basket-open').hide();
    });

}






/* PC TOPLAMA */

var pcpro = function(){

     /* Seçili olanları temizliyor*/
    var removeAll = function(elm){
        if($(elm).closest('.multiple-pro').length < 1){
            $(elm).closest('.card').find('.pc-pro-select').html('Seç');
            $(elm).closest('.card').find('.pc-pro-select').removeClass('btn-success selected').addClass('btn-primary');  
        }
    }
    
    /* Yeni elemanları seçiyor*/
    var addThis = function(elm){
        $(elm).removeClass('btn-primary').addClass('btn-success selected');
        $(elm).html('Seçildi');
    }

    /* Bir sonraki accordion'a geçiyor*/
    var goNext = function(elm){

        if($(elm).closest('.multiple-pro').length < 1 && !$(elm).closest('.card').is(':last-child')){
            $(elm).closest('.collapse').removeClass('show');
            $(elm).closest('.card').next('.card').find('.collapse').addClass('show');
        }
    }

    /* Resetleme */
    var goReset = function(elm){

        var alerts = confirm('Tüm seçimler sıfırlansın mı?');

        if(alerts === true){
            $('.pc-pro-select').removeClass('btn-success selected').addClass('btn-primary').html('Seç');

            removeAll(elm);

            addThis(elm); 

            goNext(elm);
        }
                
    }


    $(document).on('click', '.pc-pro-select', function(){

        var thisCard = $(this).closest('.card');
        var prevControl = thisCard.prev('.card').find('.selected');
        var nextControl = thisCard.next('.card').find('.selected');

        
        if(thisCard.index() > 0){
           
            if(prevControl.length == 0){
                alert('lütfen sırayla seçiniz');
            }else if(nextControl.length > 0){

                goReset(this);
    
            }else{

                removeAll(this);

                addThis(this); 

                goNext(this);

            }

        }else{


            if(nextControl.length > 0){

                goReset(this);
    
            }else{

                removeAll(this);

                addThis(this);

                goNext(this);

        }
    }

        
    });
}


$(document).ready(function () {
    libs.init();
    topSearchAction();
    btnHide();
    toppadding();
    cookieMainHide();
    navOpen();
    mbNav();
    crouselSlide();
    footerTextPreview();
    menuOverlay();
    dtPhoto.init();
    ui.init();
    components.init();
    listpageUI.init();
    hoverCarousel.init();
    cooks();
    uyeliksizSatis.init();
    basketDropdown();
    pcpro();


    var hash = window.location.hash.replace('#','');

    if(hash.length){
        console.log(hash);
    }

        
    

});






