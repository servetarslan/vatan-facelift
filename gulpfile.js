const   gulp            = require('gulp');
const   sass            = require('gulp-sass');
const   autoprefixer    = require('autoprefixer');
const   postcss         = require('gulp-postcss');
const   cssnano         = require('gulp-cssnano');
const   imagemin        = require('gulp-imagemin');
const   imageminJpegRecompress = require('imagemin-jpeg-recompress');
const   jsmin           = require('gulp-jsmin');
const   sourcemaps      = require('gulp-sourcemaps');
const   include         = require("gulp-include");
const   rename          = require("gulp-rename");
const   plumberNotifier = require('gulp-plumber-notifier');
const   notify          = require("gulp-notify");
const   concat          = require('gulp-concat');
const   htmlmin         = require('gulp-htmlmin');
const   gulpCopy        = require('gulp-copy');
const   named           = require('vinyl-named');
const   async           = require('async');
const   path            = require("path");
const   browserSync     = require('browser-sync').create();
const   reload          = browserSync.reload;


const settings = {
    appPath: "app",
    developmentPath: "dev",
    publicPath: "assets",
    htmlPath: "html",
    sassPath: "sass",
    cssPath: "css",
    jsPath: "js",
    imgPath: "images",
    viewsPath: "views",
    fontPath: "fonts",
    fontFile: "fonts.css",
    fontSettingFile: "fonts.list",
    iconFontPath: "icon-fonts",
    libSettingFile: "lib.js",
    libPath: "lib",
    libJsFileName: "lib.js",
    libCssFileName: "lib.css",
    //sourceFiles = ['images/', 'fonts/'],
    serverPort: 3000
}




const watchFiles = {
    html: [path.join(__dirname, settings.developmentPath, settings.htmlPath, "/**/*.html")],
    sass: [path.join(__dirname, settings.developmentPath, settings.publicPath, settings.sassPath, "/**/*.scss")],
    css:  [path.join(__dirname, settings.developmentPath, settings.publicPath, settings.sassPath, "/**/*.css")],
    js:   [path.join(__dirname, settings.developmentPath, settings.publicPath, settings.jsPath, "/**/main.js")],
    libJs:[path.join(__dirname, settings.developmentPath, settings.publicPath, settings.jsPath, "/**/lib.js")],
    subLibJs:[path.join(__dirname, settings.developmentPath, settings.publicPath, settings.jsPath, settings.libPath, "/**/*.js")],
    img:  [path.join(__dirname, settings.developmentPath, settings.publicPath, settings.imgPath, "/**/*.*")],

}



const DestFolder = {
    html: path.resolve(__dirname, settings.appPath),
    css: path.resolve(__dirname, settings.appPath, settings.publicPath, settings.cssPath),
    js: path.resolve(__dirname, settings.appPath, settings.publicPath, settings.jsPath),
    subJs: path.resolve(__dirname, settings.appPath, settings.publicPath, settings.jsPath, settings.libPath),
    img: path.resolve(__dirname, settings.appPath, settings.publicPath, settings.imgPath),
    libCss: path.resolve(__dirname, settings.appPath, settings.publicPath, settings.libPath, settings.cssPath),
    libJs: path.resolve(__dirname, settings.appPath, settings.publicPath, settings.jsPath),
}



gulp.task('serverLoad', function() {
    browserSync.init({
        server: {
            baseDir: [path.resolve(__dirname, settings.appPath), path.resolve(__dirname, settings.developmentPath)]
        }
    });
});



gulp.task('sass', function() {
    return gulp.src(watchFiles.sass)
        .pipe(plumberNotifier())
        .pipe(sourcemaps.init())

        .pipe(include({
            extensions: ["scss","css"],
            hardFail: true,
            includePaths: [
              __dirname + "/node_modules",
              __dirname +"/"+ settings.developmentPath +"/"+ settings.publicPath +"/"+ settings.sassPath
            ]
          }))
          
        .pipe(sass())
        
        .pipe(cssnano({
            zindex: false
        }))
        .pipe(postcss([ autoprefixer() ]))
        

        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write('./maps'))

        .pipe(gulp.dest(DestFolder.css)).on('end', function() {
            reload();
        });
});


gulp.task('css', function() {
    return gulp
        .src(watchFiles.css)
        .pipe(plumberNotifier())

        .pipe(sourcemaps.init())

        .pipe(cssnano({
            zindex: false
        }))

        /*.pipe(rename({
            suffix: ".min"
        }))*/

        .pipe(sourcemaps.write('./maps'))

        .pipe(gulp.dest(DestFolder.css))

        .pipe(notify("Minify Css Save")).on('end', function() {
            reload();
        });
});


gulp.task('js', function() {
    return gulp
        .src(watchFiles.js)
        .pipe(plumberNotifier())
        .pipe(named())
        .pipe(sourcemaps.init())

        .pipe(include())

        .pipe(jsmin())

        .pipe(rename({
            suffix: '.min'
        }))

        .pipe(sourcemaps.write('./maps'))

        .pipe(gulp.dest(DestFolder.js)).on('end', function() {
            reload();
        });
});


gulp.task('subLibJs', function() {
    return gulp
        .src(watchFiles.subLibJs)
        .pipe(plumberNotifier())
        .pipe(named())
        .pipe(sourcemaps.init())

        .pipe(include())

        .pipe(jsmin())

        .pipe(rename({
            suffix: '.min'
        }))

        .pipe(sourcemaps.write('./maps'))

        .pipe(gulp.dest(DestFolder.subJs)).on('end', function() {
            reload();
        });
});


gulp.task('libJs', function() {
    return gulp
        .src(watchFiles.libJs)
        .pipe(plumberNotifier())
        .pipe(concat(settings.libJsFileName))

        .pipe(sourcemaps.init())
        
        .pipe(include({
            extensions: "js",
            hardFail: true,
            includePaths: [
              __dirname + "/node_modules",
              __dirname +"/"+ settings.developmentPath +"/"+ settings.publicPath +"/"+ settings.jsPath
            ]
          }))

        .pipe(jsmin())

        .pipe(rename({
            suffix: '.min'
        }))

        .pipe(sourcemaps.write('./maps'))
        .pipe(notify("lib.js updated"))
        .pipe(gulp.dest(DestFolder.js)).on('end', function() {
            reload();
        });
});


gulp.task('htmlMin', () => {
    return gulp
    .src(watchFiles.html)
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest(DestFolder.html)).on('end', function() {
        reload();
    });
  });


gulp.task('imagemin', function() {
    return gulp
        .src(watchFiles.img)
        .pipe(plumberNotifier())

        .pipe(imagemin([
            imagemin.gifsicle({
                interlaced: true
            }),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imagemin.optipng({
                optimizationLevel: 8
            }),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            })
        ]))
        .pipe(gulp.dest(DestFolder.img)).on('end', function() {
            reload();
        });
});


gulp.task('watch', function() {
    gulp.watch(watchFiles.html, ['htmlMin']);
    gulp.watch(watchFiles.js, ['js']);
    gulp.watch(watchFiles.libJs, ['libJs']);
    gulp.watch(watchFiles.sass, ['sass']);
    gulp.watch(watchFiles.css, ['css']);
    gulp.watch(watchFiles.img, ['imagemin']);
    gulp.watch(watchFiles.subLibJs, ['subLibJs']);
});

gulp.task('all', [
    'htmlMin',
    'sass',
    'css',
    'js',
    'libJs',
    'subLibJs',
    'imagemin'
]);
gulp.task('default', ['serverLoad', 'imagemin', 'watch']);
